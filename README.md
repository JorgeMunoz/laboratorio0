# Laboratorio 0: introducción C++ y Python


## Integrantes
```
Jorge Munoz Taylor
Alexander Calderon Torres
Roberto Acevedo Mora
```

# PARA C++

## Como compilar el proyecto
Una vez descargado el código, ubíquese en la carpeta donde está el código:
```
>>cd {PATH}/laboratorio0
```
Luego genere el archivo MAKE:
```
>>cmake ./
```
Por último ejecute el make:
```
>>make
```

## Ejecutar el programa
Simplemente:
```
./labo0 ALGUNA_CADENA
```

# PARA Python
Ubíquese en la carpeta donde está el código:
```
>>cd {PATH}/laboratorio0
```

## Ejecutar el programa
Simplemente:
```
>>python3 Labo0.py ALGUNA_CADENA
```

## PARA Doxygen
Ubíquese en la carpeta donde está el código:
```
>>cd {PATH}/laboratorio0
```

Para compilar el doxygen:
```
>>doxygen Doxyfile
```

Para generar el archivo PDF:
```
>>cd DOCS/latex
>>make
```
Esto generará el archivo PDF con la documentación generada por doxygen. Para verlo:
```
Doble clic al archivo refman.pdf dentro de DOCS/latex
```

## Dependencias
Debe tener instalado CMAKE y MAKE en la máquina:
```
>>sudo apt-get install build-essential
>>sudo apt-get install cmake
```
También doxygen y latex:
```
>>sudo apt install doxygen
>>sudo apt install doxygen-gui
>>sudo apt-get install graphviz
>>sudo apt install texlive-latex-extra
>>sudo apt install texlive-lang-spanish
```
También debe tener python3, normalmente cualquier OS basado en Linux actual ya lo tiene.